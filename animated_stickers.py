from tgs.utils.font import FontRenderer
from tgs.utils import animation
import tgs
from tgs import objects, nvector
from tgs import Color, Point
import gzip
import json
import codecs
import os
import random

FONT_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'impact.ttf')


def closest(lst, K):
    return lst[min(range(len(lst)), key=lambda i: abs(lst[i] - K))]


# from https://github.com/LottieFiles/tgskit/blob/master/src/TGSKit.ts
def validate_and_fix(an):
    # check and fix framerate
    if an.frame_rate not in (30, 60):
        print('fixing frame rate')
        an.frame_rate = closest([30, 60], an.frame_rate)

    # check and fix frames count (no more than 180, 3 seconds)
    if (an.out_point - an.in_point) / an.frame_rate > 3:
        print('fixing duration')
        an.out_point = 180

    if an.width != 512 or an.height != 512:
        print('fixing width and height')
        an.width = 512
        an.height = 512

    if an.threedimensional and an.threedimensional != 0:
        print('fixing 3d layers')
        an.threedimensional = 0


def tg_compress(d):
    my_list = d.items() if isinstance(d, dict) else enumerate(d)

    for k, v in my_list:
        if isinstance(v, dict) or isinstance(v, list):
            # remove zero arrays

            if all([q == [0, 0] for q in d]):
                d = []
            # elif v == [0, 0]:
            #     d[k] = []
            else:
                tg_compress(v)
        else:
            if isinstance(v, float):
                # decrease float precision
                d[k] = round(v, 2)


class TextPrinter(object):
    def __init__(self, infile_path, outfile_path, selected_animation=None):
        self.infile_path = infile_path
        self.outfile_path = outfile_path
        self.tg_sticker = self._load_sticker_file(infile_path)

        self.default_animations = ('shake', 'spring_pull_top', 'spring_pull_right')
        self.selected_animation = selected_animation or random.choice(self.default_animations)

    def _load_sticker_file(self, path):
        lottie_json = tgs.parsers.tgs.parse_tgs_json(path)

        return objects.animation.Animation.load(lottie_json)

    def add_text(self, top_line, bottom_line=''):
        layer = tgs.objects.ShapeLayer()
        self.tg_sticker.insert_layer(0, layer)

        layer.add_shape(self.create_text_line(top_line))
        if bottom_line is not None and len(bottom_line):
            layer.add_shape(self.create_text_line(bottom_line, bottom=True))

        validate_and_fix(self.tg_sticker)

        output_dict = self.tg_sticker.to_dict()

        tg_compress(output_dict)

        with gzip.open(self.outfile_path, 'w') as g:
            json.dump(output_dict, codecs.getwriter('utf-8')(g), separators=(',', ':'), ensure_ascii=False)

    def create_text_line(self, text, bottom=False):
        line_group = objects.Group()

        line_shapes_group = FontRenderer(FONT_PATH).render(text, size=64, pos=nvector.NVector(0, 0))
        text_length = len(text)
        tg_seconds = self.tg_sticker.out_point / self.tg_sticker.frame_rate
        self.selected_animation = self.selected_animation if tg_seconds >= 1.5 else 'shake'

        for index, letter_shape in enumerate(line_shapes_group.shapes):
            letter_group = line_group.add_shape(objects.Group())
            letter_group.add_shape(letter_shape)
            # make letters moving
            if self.selected_animation == 'shake':
                animation.shake(letter_group.transform.position, 5, 5, 0,
                                self.tg_sticker.out_point,
                                self.tg_sticker.frame_rate//2
                                )
            elif self.selected_animation == 'spring_pull_top':
                letter_group.transform.position.value = Point(0, -512)
                end_time = self.tg_sticker.out_point // 1.7
                start_time = index * (end_time / text_length)
                if start_time < end_time:
                    animation.spring_pull(letter_group.transform.position, Point(0, 0), start_time, end_time, 7, 7)
            # elif self.selected_animation == 'spring_pull_right':
            #     letter_group.transform.position.value = Point(512 * 2, 0)
            #     total_frames_for_effect = self.tg_sticker.out_point // 1.7
            #     animation.spring_pull(letter_group.transform.position, Point(0, 0),
            #                           index * (total_frames_for_effect / text_length),
            #                           self.tg_sticker.out_point // 1.7, 7, 7)

        line_group_x = (512 - line_shapes_group.bounding_box().width) / 2
        line_group_y = 80 if not bottom else 512 - 50

        line_group.transform.position.value.x = line_group_x
        line_group.transform.position.value.y = line_group_y

        if self.selected_animation == 'spring_pull_right':
            line_group.transform.position.value.x = 512 if not bottom else -512
            animation.spring_pull(line_group.transform.position, Point(line_group_x, line_group_y),
                                  0, self.tg_sticker.out_point // 2, 7, 7)

        line_group.add_shape(objects.Fill(Color(1, 1, 1)))
        line_group.add_shape(objects.Stroke(Color(0, 0, 0), 7))

        return line_group
