# Animated Text Sticker prototype


Usage:

download impact.ttf https://www.wfonts.com/font/impact and put into project folder

```python
        import animated_stickers

        text_printer = animated_stickers.TextPrinter('<input file path>', '<output file path>')
        text_printer.add_text('Hello', 'World!')
```